NAME		:= xec
SOURCES		:= $(wildcard xec/host/*.c) \
		   $(wildcard xec/host/agent/*.c) \
		   $(wildcard xec/host/front/*.c)

DEPENDS		:= $(O)/lib/libxec-common.a
LIBS		:= $(DEPENDS)

include build/binary.mk
