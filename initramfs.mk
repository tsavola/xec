initramfs:: $(O)/initramfs.gz

$(O)/initramfs.gz: $(O)/bin/xec-slave
	test $(shell id -u) = 0
	rm -rf $(O)/initramfs
	mkdir -p $(O)/initramfs/dev
	mkdir -p $(O)/initramfs/tmp/xec-root
	mknod $(O)/initramfs/dev/console c 5 1
	mknod $(O)/initramfs/dev/fuse c 10 229
	install $(O)/bin/xec-slave $(O)/initramfs/init
	(cd $(O)/initramfs && find . | cpio -o -H newc) | gzip > $@ \
		|| (rm -f $@; false)
