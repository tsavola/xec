NAME		:= xec-slave
SOURCES		:= $(wildcard xec/slave/*.c)

LDFLAGS		+= -static

DEPENDS		:= $(O)/lib/libxec-common.a
LIBS		:= $(DEPENDS)

include build/binary.mk
