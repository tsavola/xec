/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_SIGNAL_H
#define XEC_SIGNAL_H

#include <stdint.h>

#include <xec/error.h>

typedef uint32_t	XecSignalMask;
typedef uint32_t	XecSignalToken;

typedef void (*XecSignalHandler)(int number, XecError *);

#define XEC_SIGNAL_ALL		(~(XecSignalMask) 0)
#define XEC_SIGNAL_BIT(number)	((XecSignalMask) 1 << (number))

void xec_signal_handle(XecSignalMask, XecSignalHandler, XecError *);
void xec_signal_ignore(XecError *);
void xec_signal_raise(int number);

XecSignalToken xec_signal_to_token(int number);
int xec_signal_from_token(XecSignalToken);

#endif
