/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_PACKET_H
#define XEC_PACKET_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <xec/attr.h>
#include <xec/error.h>

/*
 * packet contents (barring possible variable-length data) must consist solely
 * of 32-bit integers because they will be automatically converted to/from
 * network byte order with that assumption.  the variable-length data can be
 * anything and the client code must take care of its byte order.
 */

typedef struct XecPacket		XecPacket;
typedef struct XecPacketMessage		XecPacketMessage;
typedef struct XecPacketFuseInit	XecPacketFuseInit;
typedef struct XecPacketFuseStart	XecPacketFuseStart;
typedef struct XecPacketProcInit	XecPacketProcInit;
typedef struct XecPacketProcSet		XecPacketProcSet;
typedef struct XecPacketProcSpawn	XecPacketProcSpawn;
typedef struct XecPacketProcSignal	XecPacketProcSignal;
typedef struct XecPacketProcStatus	XecPacketProcStatus;
typedef struct XecPacketFileInit	XecPacketFileInit;
typedef struct XecPacketFileStart	XecPacketFileStart;

struct ATTR_PACKED XecPacket {
	uint32_t	size;
	uint32_t	type;
};

struct ATTR_PACKED XecPacketMessage {
	XecPacket	head;
	uint32_t	error;
	char		data[0];
};

struct ATTR_PACKED XecPacketFuseInit {
	XecPacket	head;
};

struct ATTR_PACKED XecPacketFuseStart {
	XecPacket	head;
	uint32_t	byteorder;
};

struct ATTR_PACKED XecPacketProcInit {
	XecPacket	head;
	uint32_t	front;
};

struct ATTR_PACKED XecPacketProcSet {
	XecPacket	head;
	uint32_t	key;
	uint32_t	count;
	char		data[0];
};

struct ATTR_PACKED XecPacketProcSpawn {
	XecPacket	head;
};

struct ATTR_PACKED XecPacketProcSignal {
	XecPacket	head;
	uint32_t	signal;
};

struct ATTR_PACKED XecPacketProcStatus {
	XecPacket	head;
	uint32_t	value;
	uint32_t	signal;
};

struct ATTR_PACKED XecPacketFileInit {
	XecPacket	head;
	uint32_t	front;
	uint32_t	fd;
};

struct ATTR_PACKED XecPacketFileStart {
	XecPacket	head;
};

typedef enum XecPacketType		XecPacketType;
typedef enum XecPacketByteorder		XecPacketByteorder;
typedef enum XecPacketProcKey		XecPacketProcKey;

enum XecPacketType {
	XEC_PACKET_MESSAGE,
	XEC_PACKET_FUSE_INIT,
	XEC_PACKET_FUSE_START,
	XEC_PACKET_PROC_INIT,
	XEC_PACKET_PROC_SET,
	XEC_PACKET_PROC_SPAWN,
	XEC_PACKET_PROC_SIGNAL,
	XEC_PACKET_PROC_STATUS,
	XEC_PACKET_FILE_INIT,
	XEC_PACKET_FILE_START,
};

enum XecPacketByteorder {
	XEC_PACKET_BYTEORDER_LITTLE,
	XEC_PACKET_BYTEORDER_BIG,
};

enum XecPacketProcKey {
	XEC_PACKET_PROC_SET_CWD,
	XEC_PACKET_PROC_SET_ARGS,
	XEC_PACKET_PROC_SET_ENV,
	XEC_PACKET_PROC_SET_IDS,
	XEC_PACKET_PROC_SET_RLIMITS,
};

void xec_packet_read(int fd, XecPacket *buf, size_t bufsize, XecError *);
bool ATTR_USE_RESULT xec_packet_read_eof(int fd, XecPacket *buf,
                                         size_t bufsize, XecError *);
void xec_packet_write(int fd, XecPacket *buf, XecError *);

void xec_packet_skip_unexpected(int fd, const XecPacket *, XecError *);

#endif
