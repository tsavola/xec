/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_POLL_H
#define XEC_POLL_H

#include <stdint.h>

#include <xec/attr.h>
#include <xec/error.h>

typedef	struct XecPoll	XecPoll;
typedef uint32_t	XecPollMask;

typedef void (*XecPollHandler)(XecPoll *, XecPollMask, XecError *);

struct XecPoll {
	int fd;
	XecPollHandler func;
	void *data;
};

void xec_poll_init(XecError *);
void xec_poll_loop(XecError *);
void xec_poll_quit(void);

XecPoll *xec_poll_add(int fd, XecPollMask, XecPollHandler, void *data,
                      XecError *);
void xec_poll_modify(XecPoll *, XecPollMask, XecError *);
int ATTR_USE_RESULT xec_poll_remove(XecPoll *, XecError *);
void xec_poll_destroy(XecPoll *);

void xec_poll_check(XecPollMask, XecError *);
bool ATTR_USE_RESULT xec_poll_check_eof(XecPollMask, XecError *);

#endif
