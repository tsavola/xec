/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_IO_H
#define XEC_IO_H

#include <stddef.h>

#include <xec/attr.h>
#include <xec/error.h>

void xec_io_read(int fd, void *buf, size_t size, XecError *);

bool ATTR_USE_RESULT xec_io_read_eof(int fd, void *buf, size_t size,
                                     XecError *);

size_t ATTR_USE_RESULT xec_io_read_some(int fd, void *buf, size_t bufsize,
                                        XecError *);

void xec_io_write(int fd, const void *buf, size_t size, XecError *);

void xec_io_close(int fd);

#endif
