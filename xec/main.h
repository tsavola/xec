/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_MAIN_H
#define XEC_MAIN_H

#include <xec/error.h>

void xec_main(int argc, char **argv, XecError *);

#endif
