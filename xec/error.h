/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_ERROR_H
#define XEC_ERROR_H

#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct XecError	XecError;

struct XecError {
	bool set;
};

void xec_error_vprintf_no(int number, const char *format, va_list args);

static inline void xec_error_setf_no(XecError *error, int number,
                                     const char *format, ...)
{
	va_list args;

	if (error)
		error->set = true;

	va_start(args, format);
	xec_error_vprintf_no(number, format, args);
	va_end(args);
}

static inline void xec_error_setf_errno(XecError *error,
                                        const char *format, ...)
{
	va_list args;

	if (error)
		error->set = true;

	va_start(args, format);
	xec_error_vprintf_no(-1, format, args);
	va_end(args);
}

static inline void xec_error_setf(XecError *error,
                                  const char *format, ...)
{
	va_list args;

	if (error)
		error->set = true;

	va_start(args, format);
	xec_error_vprintf_no(0, format, args);
	va_end(args);
}

static inline void xec_error_set_no(XecError *error, int number,
                                    const char *message)
{
	xec_error_setf_no(error, number, "%s", message);
}

static inline void xec_error_set_errno(XecError *error, const char *message)
{
	xec_error_set_no(error, -1, message);
}

static inline void xec_error_set(XecError *error, const char *message)
{
	xec_error_set_no(error, 0, message);
}

static inline void xec_error_no(XecError *error, int number)
{
	xec_error_setf_no(error, number, NULL);
}

static inline void xec_error_errno(XecError *error)
{
	xec_error_no(error, -1);
}

#endif
