#include "io.h"

#include <errno.h>
#include <stdbool.h>

#include <unistd.h>
#include <sys/types.h>

#include <xec/error.h>

size_t xec_io_read_some(int fd, void *buf, size_t bufsize, XecError *error)
{
	ssize_t len;

	while (true) {
		len = read(fd, buf, bufsize);
		if (len < 0) {
			if (errno == EINTR)
				continue;

			xec_error_set_errno(error, "read");
			len = 0;
		}

		return len;
	}
}

bool xec_io_read_eof(int fd, void *buf, size_t size, XecError *error)
{
	size_t len;

	len = xec_io_read_some(fd, buf, size, error);
	if (len > 0 && len < size)
		xec_error_set(error, "read: EOF");

	return len == 0;
}

void xec_io_read(int fd, void *buf, size_t size, XecError *error)
{
	if (xec_io_read_some(fd, buf, size, error) < size)
		xec_error_set(error, "read: EOF");
}

void xec_io_write(int fd, const void *buf, size_t size, XecError *error)
{
	ssize_t len;

	while (true) {
		len = write(fd, buf, size);
		if (len < 0) {
			if (errno == EINTR)
				continue;

			xec_error_set_errno(error, "write");
		} else if (len < size) {
			xec_error_set(error, "EOF");
		}

		break;
	}
}

void xec_io_close(int fd)
{
	if (close(fd) < 0)
		xec_error_set_errno(NULL, "close");
}
