/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_DEBUG_H
#define XEC_DEBUG_H

#include <stdbool.h>

#ifdef NDEBUG
# define xec_debug(args...) do {} while (0)

static inline void xec_debug_domain(const char *name)
{
}

static inline bool xec_debug_fd(int fd)
{
	return false;
}
#else
# define xec_debug(args...) xec_debug_print(__FILE__, __FUNCTION__, "" args)

void xec_debug_print(const char *, const char *, const char *, ...);
void xec_debug_domain(const char *name);

static inline bool xec_debug_fd(int fd)
{
	return fd == 1 || fd == 2;
}
#endif

#endif
