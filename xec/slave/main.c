/*
 * Copyright (c) 2008 Timo Savola
 */

#include <signal.h>

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/main.h>
#include <xec/poll.h>
#include <xec/prog.h>
#include <xec/signal.h>
#include <xec/slave/conn.h>
#include <xec/slave/proc.h>
#include <xec/slave/sandbox.h>

#define MAIN_SIGNAL_MASK \
	XEC_SIGNAL_BIT(SIGCHLD) | \
	XEC_SIGNAL_BIT(SIGINT)  | \
	XEC_SIGNAL_BIT(SIGTERM)

static void main_signal(int number, XecError *error)
{
	if (number == SIGCHLD)
		xec_slave_proc_child_exited(error);
	else
		xec_poll_quit();
}

void xec_main(int argc, char **argv, XecError *error)
{
	xec_debug_domain("slave");

	xec_poll_init(error);
	if (error->set)
		return;

	xec_signal_handle(MAIN_SIGNAL_MASK, main_signal, error);
	if (error->set)
		return;

	xec_slave_sandbox_init(error);
	if (error->set)
		return;

	xec_slave_conn_init(error);
	if (error->set)
		goto no_conn;

	xec_poll_loop(error);

no_conn:
	xec_slave_sandbox_shutdown();
}
