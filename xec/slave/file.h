/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_SLAVE_FILE_H
#define XEC_SLAVE_FILE_H

#include <xec/error.h>
#include <xec/packet.h>
#include <xec/poll.h>

typedef struct XecSlaveFile	XecSlaveFile;

struct XecSlaveFile {
	int file_fd;
	int conn_fd;

	XecSlaveFile *next;
};

void xec_slave_file_conn_inherit(XecPoll *, XecPacketFileInit *, XecError *);
void xec_slave_file_list_destroy(XecSlaveFile *list_head);
void xec_slave_file_list_redirect(XecSlaveFile *list_head, XecError *);

#endif
