/*
 * Copyright (c) 2008 Timo Savola
 */

#include "message.h"

#include <string.h>

#include <xec/error.h>
#include <xec/io.h>
#include <xec/packet.h>

void xec_slave_message_send(int fd, bool is_error, const char *data,
                            XecError *error)
{
	size_t datasize = strlen(data) + 1;
	char buf[sizeof (XecPacketMessage) + datasize];
	XecPacketMessage *packet = (XecPacketMessage *) buf;

	packet->head.size = sizeof (buf);
	packet->head.type = XEC_PACKET_MESSAGE;
	packet->error = is_error ? 1 : 0;
	memcpy(packet->data, data, datasize);

	xec_packet_write(fd, &packet->head, error);
}
