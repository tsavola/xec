/*
 * Copyright (c) 2008 Timo Savola
 */

#include "fuse.h"

#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <endian.h>
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <linux/fs.h>

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/fuse.h>
#include <xec/io.h>
#include <xec/packet.h>
#include <xec/poll.h>
#include <xec/slave/sandbox.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN
# define FUSE_BYTEORDER	XEC_PACKET_BYTEORDER_LITTLE
#elif __BYTE__ORDER == __BIG_ENDIAN
# define FUSE_BYTEORDER	XEC_PACKET_BYTEORDER_BIG
#endif

#define FUSE_DEVICE	"/dev/fuse"

static XecPoll *fuse_dev_poll;
static XecPoll *fuse_conn_poll;
static XecFuseBuffer fuse_buf;

static void fuse_conn_handle(XecPoll *poll, XecPollMask events,
                             XecError *error)
{
	size_t len;

	xec_debug();

	xec_poll_check(events, error);
	if (error->set)
		goto close;

	len = xec_io_read_some(poll->fd, fuse_buf.data, sizeof (fuse_buf),
	                       error);
	if (len == 0 || error->set)
		goto close;

	xec_io_write(fuse_conn_poll->fd, fuse_buf.data, len, error);
	if (error->set)
		goto close;

	return;

close:
	xec_poll_destroy(poll);
	fuse_conn_poll = NULL;
}

void xec_slave_fuse_conn_inherit(XecPoll *poll, XecPacketFuseInit *init,
                                 XecError *error)
{
	xec_debug();

	if (fuse_conn_poll) {
		const char *data = "FUSE connection already exists";
		char storage[sizeof (XecPacketMessage) + strlen(data) + 1];
		XecPacketMessage *packet = (XecPacketMessage *) storage;

		packet->head.size = sizeof (storage);
		packet->head.type = XEC_PACKET_MESSAGE;
		packet->error = 1;
		strcpy(packet->data, data);

		xec_packet_write(poll->fd, &packet->head, error);
		if (error->set)
			return;

		xec_error_set(error, data);
	} else {
		XecPacketFuseStart packet = {
			.head = {
				.size	= sizeof (packet),
				.type	= XEC_PACKET_FUSE_START,
			},
			.byteorder	= FUSE_BYTEORDER,
		};

		xec_packet_write(poll->fd, &packet.head, error);
		if (error->set)
			return;

		poll->func = fuse_conn_handle;
		fuse_conn_poll = poll;
	}
}

static void fuse_dev_handle(XecPoll *dev_poll, XecPollMask events,
                            XecError *error)
{
	size_t len;

	xec_debug();

	xec_poll_check(events, error);
	if (error->set)
		return;

	len = xec_io_read_some(dev_poll->fd, fuse_buf.data, sizeof (fuse_buf),
	                       error);
	if (error->set)
		return;

	if (len == 0) {
		xec_error_set(error, "EOF");
		return;
	}

	if (fuse_buf.in.opcode == FUSE_INIT) {
		struct fuse_init_in *in_init = (struct fuse_init_in *)
			(&fuse_buf.in + 1);

		struct {
			struct fuse_out_header header;
			struct fuse_init_out init;
		} out = {
			.header = {
				.len		= sizeof (out),
				.error		= 0,
				.unique		= fuse_buf.in.unique,
			},
			.init = {
				.major		= in_init->major,
				.minor		= in_init->minor,
				.max_readahead	= 0,
				.flags		= in_init->flags,
				.max_write	= sizeof (fuse_buf),
			},
		};

		xec_debug("init");
		xec_io_write(dev_poll->fd, &out, out.header.len, error);
	} else if (fuse_conn_poll) {
		xec_io_write(fuse_conn_poll->fd, fuse_buf.data, len, error);
	} else {
		struct fuse_out_header out = {
			.len		= sizeof (out),
			.error		= -EBUSY,
			.unique		= fuse_buf.in.unique,
		};

		xec_debug("no connection");
		xec_io_write(dev_poll->fd, &out, out.len, error);
	}
}

void xec_slave_fuse_mount(XecError *error)
{
	struct stat statbuf;
	int fd;
	char options[64];

	xec_debug();

	if (stat(XEC_SLAVE_SANDBOX_PATH, &statbuf) < 0) {
		xec_error_set_errno(error, XEC_SLAVE_SANDBOX_PATH);
		return;
	}

	fd = open(FUSE_DEVICE, O_RDWR);
	if (fd < 0) {
		xec_error_set_errno(error, FUSE_DEVICE);
		return;
	}

	xec_debug("fd=%d", fd);
	xec_debug("rootmode=%3o", statbuf.st_mode);

	snprintf(options, sizeof (options),
	         "fd=%d,rootmode=%o,user_id=0,group_id=0",
	         fd, statbuf.st_mode);

	if (mount("xec", XEC_SLAVE_SANDBOX_PATH, "fuse",
	          MS_SYNCHRONOUS | MS_DIRSYNC, options) < 0) {
		xec_error_set_errno(error, "fuse");
		return;
	}

	fuse_dev_poll = xec_poll_add(fd, POLLIN, fuse_dev_handle, NULL, error);
	if (error->set)
		goto no_poll;

	return;

no_poll:
	umount(XEC_SLAVE_SANDBOX_PATH);
}

void xec_slave_fuse_umount(void)
{
	xec_debug();

	umount(XEC_SLAVE_SANDBOX_PATH);
}
