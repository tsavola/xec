/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_SLAVE_PROC_H
#define XEC_SLAVE_PROC_H

#include <sys/types.h>

#include <xec/error.h>
#include <xec/poll.h>
#include <xec/slave/file.h>

typedef struct XecSlaveProc	XecSlaveProc;

struct XecSlaveProc {
	XecPoll *poll;
	unsigned int front;
	XecSlaveFile *file_list;
	char *cwd;
	char **args;
	char **env;
	pid_t pid;

	XecSlaveProc *list_prev;
	XecSlaveProc *list_next;
};

void xec_slave_proc_init(XecError *);
void xec_slave_proc_conn_inherit(XecPoll *, XecPacketProcInit *, XecError *);
void xec_slave_proc_child_exited(XecError *);
XecSlaveProc *xec_slave_proc_list_find_by_front(unsigned int front);

#endif
