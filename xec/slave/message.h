/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_SLAVE_MESSAGE_H
#define XEC_SLAVE_MESSAGE_H

#include <xec/error.h>

void xec_slave_message_send(int fd, bool is_error, const char *, XecError *);

#endif
