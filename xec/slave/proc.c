/*
 * Copyright (c) 2008 Timo Savola
 */

#define _POSIX_SOURCE

#include "proc.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

extern char **environ;

#include <xec/debug.h>
#include <xec/io.h>
#include <xec/packet.h>
#include <xec/poll.h>
#include <xec/signal.h>
#include <xec/slave/file.h>
#include <xec/slave/sandbox.h>

static XecSlaveProc *proc_list;

static void proc_list_add(XecSlaveProc *proc)
{
	xec_debug("proc=%p", proc);

	proc->list_prev = NULL;
	proc->list_next = proc_list;
	proc_list = proc;
}

static void proc_list_remove(XecSlaveProc *proc)
{
	xec_debug("proc=%p", proc);

	if (proc->list_prev)
		proc->list_prev->list_next = proc->list_next;

	if (proc->list_next)
		proc->list_next->list_prev = proc->list_prev;

	if (proc == proc_list)
		proc_list = proc->list_next;

	proc->list_prev = NULL;
	proc->list_next = NULL;
}

XecSlaveProc *xec_slave_proc_list_find_by_front(unsigned int front)
{
	XecSlaveProc *proc;

	xec_debug("front=%u", front);

	for (proc = proc_list; proc; proc = proc->list_next)
		if (proc->front == front)
			break;

	xec_debug("proc=%p", proc);

	return proc;
}

static XecSlaveProc *proc_list_find_by_pid(pid_t pid)
{
	XecSlaveProc *proc;

	xec_debug("pid=%d", pid);

	for (proc = proc_list; proc; proc = proc->list_next)
		if (proc->pid == pid)
			break;

	xec_debug("proc=%p", proc);

	return proc;
}

static void proc_destroy_child_data(XecSlaveProc *proc)
{
	xec_slave_file_list_destroy(proc->file_list);
	proc->file_list = NULL;

	free(proc->env);
	proc->env = NULL;

	free(proc->args);
	proc->args = NULL;

	free(proc->cwd);
	proc->cwd = NULL;
}

static void proc_destroy(XecSlaveProc *proc)
{
	xec_debug("proc=%p", proc);

	proc_list_remove(proc);

	proc_destroy_child_data(proc);
	xec_poll_destroy(proc->poll);
	free(proc);
}

static void proc_send_status(XecPoll *poll, int value, int signal,
                             XecError *error)
{
	XecPacketProcStatus packet = {
		.head = {
			.size	= sizeof (packet),
			.type	= XEC_PACKET_PROC_STATUS,
		},
		.value		= value,
		.signal		= xec_signal_to_token(signal),
	};

	xec_debug();

	xec_packet_write(poll->fd, &packet.head, error);
}

static char **proc_make_strings(size_t length, char *data, size_t datasize,
                                XecError *error)
{
	char **vec = NULL;
	size_t offset = 0;
	int i;

	vec = malloc((length + 1) * sizeof (char *));
	if (vec == NULL) {
		xec_error_errno(error);
		goto fail;
	}

	for (i = 0; i < length; i++) {
		vec[i] = data + offset;
		offset += strlen(data + offset) + 1;

		if (offset > datasize) {
			xec_error_set(error, "vector length exceeds contents");
			goto fail;
		}
	}
	vec[i] = NULL;

	return vec;

fail:
	free(vec);
	return NULL;
}

static void proc_handle_set(XecPoll *poll, XecPacketProcSet *packet,
                            XecError *error)
{
	XecSlaveProc *proc = poll->data;
	size_t size;
	char *buf = NULL;

	xec_debug();

	if (proc->pid) {
		xec_error_set(error, "child already running");
		goto fail;
	}

	size = packet->head.size - sizeof (XecPacketProcSet);
	if (size <= 1) {
		xec_error_set(error, "proc set packet too small");
		goto fail;
	}

	buf = malloc(size);
	if (buf == NULL) {
		xec_error_errno(error);
		goto fail;
	}

	xec_io_read(poll->fd, buf, size, error);
	if (error->set)
		goto fail;

	switch (packet->key) {
	case XEC_PACKET_PROC_SET_CWD:
		if (proc->cwd) {
			xec_error_set(error, "process cwd already set");
			goto fail;
		}

		if (packet->count != 1) {
			xec_error_set(error, "proc set cwd count != 1");
			goto fail;
		}

		xec_debug("cwd=\"%s\"", buf);
		proc->cwd = buf;
		break;

	case XEC_PACKET_PROC_SET_ARGS:
		if (proc->args) {
			xec_error_set(error, "process args already set");
			goto fail;
		}

		if (packet->count == 0) {
			xec_error_set(error, "proc set args count == 0");
			goto fail;
		}

		proc->args = proc_make_strings(packet->count, buf, size,
		                               error);
		break;

	case XEC_PACKET_PROC_SET_ENV:
		if (proc->env) {
			xec_error_set(error, "process env already set");
			goto fail;
		}

		proc->env = proc_make_strings(packet->count, buf, size, error);
		break;

	case XEC_PACKET_PROC_SET_IDS:
		/* TODO: ids */
		goto fail;

	case XEC_PACKET_PROC_SET_RLIMITS:
		/* TODO: rlimits */
		goto fail;

	default:
		xec_error_setf(error, "unknown proc key: 0x%08x", packet->key);
		goto fail;
	}

	return;

fail:
	free(buf);
}

static void proc_handle_spawn(XecPoll *poll, XecPacketProcSpawn *packet,
                              XecError *error)
{
	XecSlaveProc *proc = poll->data;
	pid_t pid;

	xec_debug();

	pid = fork();
	if (pid < 0) {
		xec_error_set_errno(error, "fork");
		return;
	}

	if (pid == 0) {
		xec_debug_domain("child");

		xec_slave_sandbox_chroot(error);
		if (error->set)
			goto fail;

		xec_debug("cwd=\"%s\"", proc->cwd);
		if (proc->cwd && chdir(proc->cwd) < 0) {
			xec_error_set_errno(error, proc->cwd);
			goto fail;
		}

		if (proc->args == NULL) {
			xec_error_set(error, "process args not set");
			goto fail;
		}

		for (int i = 0; proc->args[i]; i++)
			xec_debug("arg%d=\"%s\"", i, proc->args[i]);

		if (proc->env) {
			environ = proc->env;
		} else {
			static char *empty[] = { NULL };
			environ = empty;
		}

		xec_debug("PATH=\"%s\"", getenv("PATH"));

		xec_slave_file_list_redirect(proc->file_list, error);
		if (error->set)
			goto fail;

		execvp(proc->args[0], proc->args);
		xec_error_set_errno(error, proc->args[0]);

	fail:
		exit(127);
	}

	xec_debug("pid=%d", pid);

	proc->pid = pid;

	proc_destroy_child_data(proc);
}

static void proc_handle_signal(XecPoll *poll, XecPacketProcSignal *packet,
                               XecError *error)
{
	XecSlaveProc *proc = poll->data;
	int signal;

	xec_debug();

	signal = xec_signal_from_token(packet->signal);
	if (signal < 0) {
		xec_error_setf(error, "unknown signal token (0x%08x)",
		               packet->signal);
		return;
	}

	if (proc->pid) {
		xec_debug("kill");
		if (kill(proc->pid, signal) < 0)
			xec_error_set_errno(error, "kill");
	} else {
		xec_debug("child not running");
		proc_send_status(poll, 0, signal, error);
	}
}

static void proc_conn_handle(XecPoll *poll, XecPollMask events,
                             XecError *global_error)
{
	XecError error = {};
	XecSlaveProc *proc = poll->data;
	union {
		XecPacket head;
		XecPacketProcSet set;
		XecPacketProcSpawn spawn;
		XecPacketProcSignal signal;
	} packet;

	xec_debug();

	xec_poll_check(events, &error);
	if (error.set)
		goto fail;

	xec_packet_read(poll->fd, &packet.head, sizeof (packet), &error);
	if (error.set)
		goto fail;

	switch (packet.head.type) {
	case XEC_PACKET_PROC_SET:
		proc_handle_set(poll, &packet.set, &error);
		break;

	case XEC_PACKET_PROC_SPAWN:
		proc_handle_spawn(poll, &packet.spawn, &error);
		break;

	case XEC_PACKET_PROC_SIGNAL:
		proc_handle_signal(poll, &packet.signal, &error);
		break;

	default:
		xec_packet_skip_unexpected(poll->fd, &packet.head, &error);
		break;
	}

fail:
	if (error.set) {
		if (proc->pid && kill(proc->pid, SIGKILL) < 0)
			xec_error_set_errno(&error, "kill");

		proc_destroy(proc);
	}
}

void xec_slave_proc_conn_inherit(XecPoll *poll, XecPacketProcInit *packet,
                                 XecError *error)
{
	XecSlaveProc *proc;

	xec_debug("front=%u", packet->front);

	proc = calloc(1, sizeof (XecSlaveProc));
	if (proc == NULL) {
		xec_error_errno(error);
		return;
	}

	proc->poll = poll;
	proc->front = packet->front;

	proc_list_add(proc);

	poll->func = proc_conn_handle;
	poll->data = proc;
}

void xec_slave_proc_child_exited(XecError *error)
{
	pid_t pid;
	int status;
	XecSlaveProc *proc;
	int value = 0;
	int signal = 0;

	xec_debug("wait");

	pid = wait(&status);
	if (pid < 0) {
		xec_error_set_errno(error, "wait");
		return;
	}

	if (WIFEXITED(status))
		value = WEXITSTATUS(status);
	else
		signal = WTERMSIG(status);

	xec_debug("pid=%d value=%d signal=%d", pid, value, signal);

	proc = proc_list_find_by_pid(pid);
	if (proc) {
		proc_send_status(proc->poll, value, signal, error);
		proc_destroy(proc);
	}
}
