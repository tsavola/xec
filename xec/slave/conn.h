/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_SLAVE_CONN_H
#define XEC_SLAVE_CONN_H

#include <xec/error.h>

void xec_slave_conn_init(XecError *);

#endif
