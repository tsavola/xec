/*
 * Copyright (c) 2008 Timo Savola
 */

#include "file.h"

#include <stddef.h>
#include <stdlib.h>

#include <unistd.h>

#include <xec/debug.h>
#include <xec/io.h>
#include <xec/packet.h>
#include <xec/slave/proc.h>

static void file_list_add(XecSlaveFile **list, XecSlaveFile *file)
{
	file->next = *list;
	*list = file;
}

void xec_slave_file_list_destroy(XecSlaveFile *list_head)
{
	XecSlaveFile *file;
	XecSlaveFile *next;

	for (file = list_head; file; file = next) {
		next = file->next;

		xec_io_close(file->conn_fd);
		free(file);
	}
}

static void file_send_start(int conn_fd, XecError *error)
{
	XecPacketFileStart packet = {
		.head = {
			.size	= sizeof (packet),
			.type	= XEC_PACKET_FILE_START,
		},
	};

	xec_debug();
	xec_packet_write(conn_fd, &packet.head, error);
}

void xec_slave_file_conn_inherit(XecPoll *poll, XecPacketFileInit *packet,
                                 XecError *error)
{
	XecSlaveProc *proc;
	XecSlaveFile *file;

	xec_debug();

	proc = xec_slave_proc_list_find_by_front(packet->front);
	if (proc == NULL) {
		xec_error_setf(error, "invalid front: %u", packet->front);
		return;
	}

	file = malloc(sizeof (XecSlaveFile));
	if (file == NULL) {
		xec_error_errno(error);
		return;
	}

	file_send_start(poll->fd, error);
	if (error->set)
		return;

	file_list_add(&proc->file_list, file);

	file->file_fd = packet->fd;
	file->conn_fd = xec_poll_remove(poll, error);

	xec_debug("file_fd=%d", file->file_fd);
	xec_debug("conn_fd=%d", file->conn_fd);
}

void xec_slave_file_list_redirect(XecSlaveFile *list_head, XecError *error)
{
	XecSlaveFile *file;

	xec_debug();

	/* TODO: relocate and close arbitrary file descriptors */

	for (file = list_head; file; file = file->next)
		if (dup2(file->conn_fd, file->file_fd) < 0) {
			/* this error might go to the redirected stderr */
			xec_error_set_errno(error, "dup2");
			break;
		}
}
