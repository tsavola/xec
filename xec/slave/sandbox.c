/*
 * Copyright (c) 2008 Timo Savola
 */

#define _GNU_SOURCE

#include "sandbox.h"

#include <stddef.h>

#include <unistd.h>
#include <sys/mount.h>
#include <sys/stat.h>

#include <xec/array.h>
#include <xec/debug.h>
#include <xec/error.h>
#include <xec/slave/fuse.h>

typedef struct SandboxMount	SandboxMount;

struct SandboxMount {
	const char *target;
	const char *fstype;
};

static const SandboxMount sandbox_mount_table[] = {
	{ XEC_SLAVE_SANDBOX_PATH "/dev",     "tmpfs"  },
	{ XEC_SLAVE_SANDBOX_PATH "/dev/pts", "devpts" },
	{ XEC_SLAVE_SANDBOX_PATH "/proc",    "proc"   },
	{ XEC_SLAVE_SANDBOX_PATH "/sys",     "sysfs"  },
};

static void sandbox_mount(const SandboxMount *point, XecError *error)
{
	xec_debug("target=\"%s\"", point->target);

#if 0
	if (access(point->target, F_OK) < 0 &&
	    mkdir(point->target, 0755) < 0) {
		xec_error_set_errno(error, point->target);
		return;
	}

	if (mount("none", point->target, point->fstype, 0, NULL) < 0)
		xec_error_set_errno(error, point->target);
#endif
}

static void sandbox_umount(const SandboxMount *point)
{
#if 0
	umount(point->target);
#endif
}

void xec_slave_sandbox_init(XecError *error)
{
	int index;

	xec_debug();

	xec_slave_fuse_mount(error);
	if (error->set)
		return;

	for (index = 0; index < ARRAY_SIZE(sandbox_mount_table); index++) {
		sandbox_mount(&sandbox_mount_table[index], error);
		if (error->set)
			goto no_mount;
	}

	return;

no_mount:
	while (index-- > 0)
		sandbox_umount(&sandbox_mount_table[index]);

	xec_slave_fuse_umount();
}

void xec_slave_sandbox_shutdown(void)
{
	xec_debug();

	for (int i = ARRAY_SIZE(sandbox_mount_table); i-- > 0; )
		sandbox_umount(&sandbox_mount_table[i]);

	xec_slave_fuse_umount();
}

void xec_slave_sandbox_chroot(XecError *error)
{
	xec_debug();

	if (chroot(XEC_SLAVE_SANDBOX_PATH) < 0) {
		xec_error_set_errno(error, "chroot");
		return;
	}

	if (chdir("/") < 0)
		xec_error_set_errno(error, "chdir");
}
