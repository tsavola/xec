/*
 * Copyright (c) 2008 Timo Savola
 */

#include "conn.h"

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <poll.h>
#include <unistd.h>

#include <xec/array.h>
#include <xec/debug.h>
#include <xec/error.h>
#include <xec/io.h>
#include <xec/packet.h>
#include <xec/poll.h>
#include <xec/slave/fuse.h>
#include <xec/slave/message.h>
#include <xec/slave/proc.h>

#define CONN_PORT	1203

static void conn_handle_handshake(XecPoll *poll, XecPollMask events,
                                  XecError *global_error)
{
	XecError error = {};
	union {
		XecPacket head;
		XecPacketFuseInit fuse;
		XecPacketProcInit proc;
		XecPacketFileInit file;
	} packet;

	xec_debug();

	xec_poll_check(events, &error);
	if (error.set)
		goto fail;

	xec_packet_read(poll->fd, &packet.head, sizeof (packet), &error);
	if (error.set)
		goto fail;

	switch (packet.head.type) {
	case XEC_PACKET_FUSE_INIT:
		xec_slave_fuse_conn_inherit(poll, &packet.fuse, &error);
		break;

	case XEC_PACKET_PROC_INIT:
		xec_slave_proc_conn_inherit(poll, &packet.proc, &error);
		break;

	case XEC_PACKET_FILE_INIT:
		xec_slave_file_conn_inherit(poll, &packet.file, &error);
		break;

	default:
		xec_packet_skip_unexpected(poll->fd, &packet.head, &error);
		break;
	}

fail:
	if (poll->fd < 0 || error.set) {
		if (poll->fd >= 0)
			xec_slave_message_send(poll->fd, true, "fail", &error);

		xec_poll_destroy(poll);
	}
}

static void conn_listen_handle(XecPoll *listen_poll, XecPollMask events,
                               XecError *global_error)
{
	XecError error = {};
	struct sockaddr_storage addr;
	socklen_t addrlen = sizeof (addr);
	int conn_fd;

	xec_debug();

	xec_poll_check(events, global_error);
	if (global_error->set)
		return;

	conn_fd = accept(listen_poll->fd, (struct sockaddr *) &addr, &addrlen);
	if (conn_fd < 0) {
		xec_error_set_errno(&error, "accept");
		return;
	}

	xec_debug("conn_fd=%d", conn_fd);

	xec_poll_add(conn_fd, POLLIN, conn_handle_handshake, NULL, &error);
	if (error.set)
		goto no_poll;

	return;

no_poll:
	xec_io_close(conn_fd);
}

/*
 * don't use addrinfo because we can't link statically against glibc if we do.
 */
void xec_slave_conn_init(XecError *error)
{
	struct sockaddr_in addr = {
		.sin_port = htons(CONN_PORT),
		.sin_addr = { .s_addr = htonl(INADDR_ANY) },
	};
	int fd;
	int val;

	fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0) {
		xec_error_set_errno(error, "socket");
		return;
	}

	xec_debug("fd=%d", fd);

	val = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof (val)) < 0) {
		xec_error_set_errno(error, "setsockopt SO_REUSEADDR");
		return;
	}

	if (bind(fd, (struct sockaddr *) &addr, sizeof (addr)) < 0) {
		xec_error_set_errno(error, "bind");
		return;
	}

	if (listen(fd, SOMAXCONN) < 0) {
		xec_error_set_errno(error, "listen");
		return;
	}

	xec_poll_add(fd, POLLIN, conn_listen_handle, NULL, error);
}
