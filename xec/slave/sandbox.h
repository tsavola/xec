/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_SLAVE_SANDBOX_H
#define XEC_SLAVE_SANDBOX_H

#include <xec/error.h>

#define XEC_SLAVE_SANDBOX_PATH	"/tmp/xec-root"

void xec_slave_sandbox_init(XecError *);
void xec_slave_sandbox_shutdown(void);

void xec_slave_sandbox_chroot(XecError *);

#endif
