/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_SLAVE_FUSE_H
#define XEC_SLAVE_FUSE_H

#include <xec/error.h>
#include <xec/packet.h>
#include <xec/poll.h>

void xec_slave_fuse_mount(XecError *);
void xec_slave_fuse_umount(void);
void xec_slave_fuse_conn_inherit(XecPoll *, XecPacketFuseInit *, XecError *);

#endif
