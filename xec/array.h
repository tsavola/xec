/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_ARRAY_H
#define XEC_ARRAY_H

#define ARRAY_SIZE(array)	(sizeof (array) / sizeof ((array)[0]))

#endif
