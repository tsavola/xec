/*
 * Copyright (c) 2008 Timo Savola
 */

#include "packet.h"

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#include <arpa/inet.h>

#include <xec/array.h>
#include <xec/debug.h>
#include <xec/error.h>
#include <xec/io.h>
#include <xec/prog.h>

typedef const struct PacketInfo	PacketInfo;

struct PacketInfo {
	size_t size;
	bool extensible;
};

static PacketInfo packet_info_table[] = {
	[XEC_PACKET_MESSAGE]     = { sizeof (XecPacketMessage),    true  },
	[XEC_PACKET_FUSE_INIT]   = { sizeof (XecPacketFuseInit),   false },
	[XEC_PACKET_FUSE_START]  = { sizeof (XecPacketFuseStart),  false },
	[XEC_PACKET_PROC_INIT]   = { sizeof (XecPacketProcInit),   false },
	[XEC_PACKET_PROC_SET]    = { sizeof (XecPacketProcSet),    true  },
	[XEC_PACKET_PROC_SPAWN]  = { sizeof (XecPacketProcSpawn),  false },
	[XEC_PACKET_PROC_SIGNAL] = { sizeof (XecPacketProcSignal), false },
	[XEC_PACKET_PROC_STATUS] = { sizeof (XecPacketProcStatus), false },
	[XEC_PACKET_FILE_INIT]   = { sizeof (XecPacketFileInit),   false },
	[XEC_PACKET_FILE_START]  = { sizeof (XecPacketFileStart),  false },
};

static inline PacketInfo *packet_info(const XecPacket *packet)
{
	return &packet_info_table[packet->type];
}

static void packet_skip(int fd, size_t remain, XecError *error)
{
	xec_debug("skipping %u bytes", remain);

	while (remain > 0) {
		char buf[64];
		size_t len;

		if (remain > sizeof (buf))
			len = sizeof (buf);
		else
			len = remain;

		xec_io_read(fd, buf, len, error);
		if (error->set)
			break;

		remain -= len;
	}
}

static PacketInfo *packet_validate_input(int fd, XecPacket *packet,
                                         XecError *error)
{
	PacketInfo *info;
	bool bad;

	if (packet->type >= ARRAY_SIZE(packet_info_table) ||
	    packet_info(packet)->size == 0) {
		xec_prog_messagef("skipping unknown packet (0x%08x)",
		                  packet->type);

		packet_skip(fd, packet->size, error);
		return NULL;
	}

	info = packet_info(packet);
	if (info->extensible)
		bad = packet->size < info->size;
	else
		bad = packet->size != info->size;

	if (bad)
		xec_error_setf(error,
			"packet (type 0x%08x) has bad packet size: 0x%08x",
			packet->type, packet->size);

	return info;
}

static PacketInfo *packet_validate_output(XecPacket *packet)
{
	PacketInfo *info;

	assert(packet->type < ARRAY_SIZE(packet_info_table));

	info = packet_info(packet);
	assert(info->size);

	if (info->extensible)
		assert(packet->size >= info->size);
	else
		assert(packet->size == info->size);

	return info;
}

static void packet_hton(void *buf, size_t size)
{
	uint32_t *contents = buf;
	int count = size / sizeof (uint32_t);

	for (int i = 0; i < count; i++)
		contents[i] = htonl(contents[i]);
}

static void packet_ntoh(void *buf, size_t size)
{
	uint32_t *contents = buf;
	int count = size / sizeof (uint32_t);

	for (int i = 0; i < count; i++)
		contents[i] = ntohl(contents[i]);
}

void xec_packet_read(int fd, XecPacket *buf, size_t bufsize, XecError *error)
{
	if (xec_packet_read_eof(fd, buf, bufsize, error))
		xec_error_set(error, "packet read: EOF");
}

bool xec_packet_read_eof(int fd, XecPacket *buf, size_t bufsize, XecError *error)
{
	bool eof = false;
	PacketInfo *info;
	void *tail;
	size_t tailsize;

	xec_debug("fd=%d", fd);
	assert((bufsize & 3) == 0);

	eof = xec_io_read_eof(fd, buf, sizeof (XecPacket), error);
	if (eof || error->set)
		return eof;

	packet_ntoh(buf, sizeof (XecPacket));

	xec_debug("size=0x%08x", buf->size);
	xec_debug("type=0x%08x", buf->type);

	info = packet_validate_input(fd, buf, error);
	if (info == NULL || error->set)
		return eof;

	if (bufsize >= info->size)
		bufsize = info->size;
	else
		xec_debug("buffer too small (%lu)", bufsize);

	if (bufsize == sizeof (XecPacket))
		return eof;

	tail = buf + 1;
	tailsize = bufsize - sizeof (XecPacket);

	xec_io_read(fd, tail, tailsize, error);
	if (error->set)
		return eof;

	packet_ntoh(tail, tailsize);
	return eof;
}

void xec_packet_write(int fd, XecPacket *buf, XecError *error)
{
	size_t bufsize = buf->size;
	PacketInfo *info;

	xec_debug("fd=%d", fd);
	xec_debug("size=0x%08x", buf->size);
	xec_debug("type=0x%08x", buf->type);

	info = packet_validate_output(buf);
	packet_hton(buf, info->size);
	xec_io_write(fd, buf, bufsize, error);
}

void xec_packet_skip_unexpected(int fd, const XecPacket *packet,
                                XecError *error)
{
	PacketInfo *info = packet_info(packet);

	xec_prog_messagef("skipping unexpected packet (0x%08x)", packet->type);

	if (info->extensible)
		packet_skip(fd, packet->size - info->size, error);
}
