/*
 * Copyright (c) 2008 Timo Savola
 */

#include "prog.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xec/error.h>
#include <xec/main.h>

static char *prog_name;

const char *xec_prog_name(void)
{
	return prog_name;
}

void xec_prog_usage(const char *args)
{
	fprintf(stderr, "usage: %s %s\n", prog_name, args);
}

void xec_prog_messagef(const char *format, ...)
{
	va_list args;

	fprintf(stderr, "%s: ", prog_name);

	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);

	fprintf(stderr, "\n");
}

int main(int argc, char **argv)
{
	XecError error = {};
	char *tail;

	tail = strrchr(argv[0], '/');
	if (tail)
		prog_name = tail + 1;
	else
		prog_name = argv[0];

	xec_main(argc - 1, argv + 1, &error);

	return error.set ? EXIT_FAILURE : EXIT_SUCCESS;
}
