/*
 * Copyright (c) 2008 Timo Savola
 */

#include "error.h"

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include <xec/debug.h>
#include <xec/prog.h>

void xec_error_vprintf_no(int number, const char *format, va_list args)
{
	if (number < 0)
		number = errno;

	fprintf(stderr, "%s: error: ", xec_prog_name());

	if (format)
		vfprintf(stderr, format, args);

	if (number)
		fprintf(stderr, format ? ": %s" : "%s", strerror(number));

	fprintf(stderr, "\n");
}
