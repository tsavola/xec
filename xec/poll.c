/*
 * Copyright (c) 2008 Timo Savola
 */

#include "poll.h"

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#include <poll.h>
#include <unistd.h>

#include <sys/epoll.h>

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/io.h>

static int poll_fd;
static int poll_count;
static bool poll_quit;

void xec_poll_init(XecError *error)
{
	xec_debug();

	poll_fd = epoll_create(2);
	if (poll_fd < 0)
		xec_error_errno(error);
}

void xec_poll_loop(XecError *error)
{
	struct epoll_event event;
	XecPoll *poll;

	xec_debug();

	while (poll_count > 0 && !poll_quit && !error->set) {
		xec_debug("poll_count=%d wait", poll_count);

		if (epoll_wait(poll_fd, &event, 1, -1) < 0) {
			if (errno == EINTR)
				continue;

			xec_error_errno(error);
			break;
		}

		poll = event.data.ptr;

		xec_debug("poll=%p fd=%d events=0x%x", poll, poll->fd,
		          event.events);

		poll->func(poll, event.events, error);
	}

	xec_debug("exit");
}

void xec_poll_quit(void)
{
	xec_debug();

	poll_quit = true;
}

static void poll_control(XecPoll *poll, int operation, XecPollMask events,
                         XecError *error)
{
	struct epoll_event event = {
		.events = events,
		.data = { .ptr = poll },
	};

	xec_debug("fd=%d", poll->fd);

	if (epoll_ctl(poll_fd, operation, poll->fd, &event) < 0)
		xec_error_errno(error);
}

XecPoll *xec_poll_add(int fd, XecPollMask events, XecPollHandler func,
                      void *data, XecError *error)
{
	XecPoll *poll;

	poll = malloc(sizeof (XecPoll));
	if (poll == NULL) {
		xec_error_errno(error);
		goto no_alloc;
	}

	xec_debug("poll=%p events=0x%x func=%p", poll, events, func);

	poll->fd = fd;
	poll->func = func;
	poll->data = data;

	poll_control(poll, EPOLL_CTL_ADD, events, error);
	if (error->set)
		goto no_control;

	poll_count++;
	xec_debug("poll_count=%d", poll_count);
	return poll;

no_control:
	free(poll);
no_alloc:
	return NULL;
}

void xec_poll_modify(XecPoll *poll, XecPollMask events, XecError *error)
{
	xec_debug("poll=%p events=0x%x", poll, events);

	poll_control(poll, EPOLL_CTL_MOD, events, error);
	if (error->set)
		poll_control(poll, EPOLL_CTL_DEL, events, NULL);
}

int xec_poll_remove(XecPoll *poll, XecError *error)
{
	int fd = poll->fd;

	xec_debug("poll=%p", poll);

	poll_count--;
	xec_debug("poll_count=%d", poll_count);

	poll_control(poll, EPOLL_CTL_DEL, 0, error);
	poll->fd = -1;

	return fd;
}

/*
 * epoll(7) says that a file descriptor is automatically removed from all epoll
 * fd sets when it's closed.  epoll_ctl(EPOLL_CTL_DEL) for a closed fd indeed
 * gives an error, but epoll_wait still reports events from it!  so we'll
 * delete and close the fd manually..
 */
void xec_poll_destroy(XecPoll *poll)
{
	xec_debug("poll=%p", poll);

	if (poll->fd >= 0) {
		poll_count--;
		xec_debug("poll_count=%d", poll_count);

		poll_control(poll, EPOLL_CTL_DEL, 0, NULL);
		xec_io_close(poll->fd);
	}

	free(poll);
}

void xec_poll_check(XecPollMask events, XecError *error)
{
	if (xec_poll_check_eof(events, error))
		xec_error_set(error, "poll: hang up");
}

bool xec_poll_check_eof(XecPollMask events, XecError *error)
{
	if (events & POLLERR)
		xec_error_set(error, "poll: error");

	if (events & POLLNVAL)
		xec_error_set(error, "poll: invalid file descriptor");

	if (events & ~(POLLIN | POLLOUT | POLLERR | POLLHUP | POLLNVAL))
		xec_error_set(error, "poll: unexpected condition");

	return (events & POLLHUP) != 0;
}
