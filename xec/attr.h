/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_ATTR_H
#define XEC_ATTR_H

#define ATTR_PACKED	__attribute__ ((packed))
#define ATTR_USE_RESULT	__attribute__ ((warn_unused_result))

#endif
