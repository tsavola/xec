/*
 * Copyright (c) 2008 Timo Savola
 */

#include "debug.h"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <sys/time.h>

#ifndef NDEBUG

#define DEBUG_FUNC_WIDTH	31

static time_t debug_start_sec;
static const char *debug_domain;

void xec_debug_domain(const char *name)
{
	debug_domain = name;
}

void xec_debug_print(const char *file, const char *func,
                     const char *format, ...)
{
	struct timeval tv;
	int funclen = 0;

	gettimeofday(&tv, NULL);

	if (debug_start_sec == 0)
		debug_start_sec = tv.tv_sec / 100 * 100;

	printf("%2ld.%02ld  %s  ", tv.tv_sec - debug_start_sec,
	       tv.tv_usec / 10000, debug_domain);

	if (strncmp(func, "xec_", 4) == 0) {
		funclen += printf("%s", func + 4);
	} else if (strncmp(file, "xec/", 4) == 0) {
		char *end;

		end = strrchr(file + 4, '/');
		if (end)
			funclen += fwrite(file + 4, 1, end - file - 3, stdout);

		funclen += printf("%s", func);
	} else {
		funclen += printf("%s %s", file, func);
	}

	if (format[0]) {
		va_list args;

		funclen += printf("  ");

		for (; funclen < DEBUG_FUNC_WIDTH; funclen++)
			putchar(' ');

		va_start(args, format);
		vprintf(format, args);
		va_end(args);
	}

	putchar('\n');
}

#endif
