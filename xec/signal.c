/*
 * Copyright (c) 2008 Timo Savola
 */

#define _POSIX_SOURCE

#include "signal.h"

#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <poll.h>
#include <unistd.h>
#include <sys/types.h>

#include <xec/array.h>
#include <xec/debug.h>
#include <xec/error.h>
#include <xec/io.h>
#include <xec/main.h>
#include <xec/poll.h>

#define SIGNAL_MIN	 1
#define SIGNAL_MAX	31

static int signal_catch_fd = -1;
static XecPoll *signal_handle_poll;
static XecSignalHandler signal_handle_func;

static void signal_catch(int number)
{
	XecError error = {};

	xec_debug("number=%d", number);

	xec_io_write(signal_catch_fd, &number, sizeof (number), &error);
	if (error.set)
		abort();
}

static void signal_handle(XecPoll *poll, XecPollMask events, XecError *error)
{
	int number;

	xec_poll_check(events, error);
	if (error->set)
		return;

	xec_io_read(poll->fd, &number, sizeof (number), error);
	if (error->set)
		return;

	xec_debug("number=%d", number);

	signal_handle_func(number, error);
}

static void signal_configure(XecSignalMask mask, XecError *error)
{
	struct sigaction action = {
		.sa_flags	= SA_NOCLDSTOP,
	};
	int number;
	int retval;

	xec_debug("mask=0x%08x", mask);

	for (number = SIGNAL_MIN; number <= SIGNAL_MAX; number++) {
		switch (number) {
		case SIGABRT:
		case SIGBUS:
		case SIGCONT:
		case SIGFPE:
		case SIGILL:
		case SIGKILL:
		case SIGSEGV:
		case SIGSTOP:
			continue;

		default:
			break;
		}

		if (mask & XEC_SIGNAL_BIT(number)) {
			action.sa_handler = signal_catch;
			retval = sigaction(number, &action, NULL);
		} else {
			action.sa_handler = SIG_IGN;
			retval = sigaction(number, &action, NULL);
		}

		if (retval < 0)
			xec_error_setf_errno(error,
			                     "failed to configure signal %d",
			                     number);
	}
}

void xec_signal_handle(XecSignalMask mask, XecSignalHandler func,
                       XecError *error)
{
	int fd[2];

	xec_debug("mask=0x%08x func=%p", mask, func);

	if (signal_catch_fd < 0) {
		if (pipe(fd) < 0) {
			xec_error_errno(error);
			return;
		}

		signal_handle_poll = xec_poll_add(fd[0], POLLIN, signal_handle,
		                                  NULL, error);
		if (error->set)
			return;

		signal_catch_fd = fd[1];
	}

	signal_handle_func = func;

	signal_configure(mask, error);
}

void xec_signal_ignore(XecError *error)
{
	xec_debug();

	signal_configure(0, error);

	if (signal_catch_fd >= 0) {
		xec_io_close(signal_catch_fd);
		xec_poll_destroy(signal_handle_poll);

		signal_catch_fd = -1;
		signal_handle_poll = NULL;
	}

	signal_handle_func = NULL;
}

void xec_signal_raise(int number)
{
	struct sigaction action = {
		.sa_handler	= SIG_DFL,
	};

	xec_debug("number=%d", number);

	if (sigaction(number, &action, NULL) < 0) {
		xec_error_setf_errno(NULL, "failed to reset signal %d",
		                     number);
		number = SIGKILL;
	}

	if (raise(number) < 0) {
		xec_error_setf_errno(NULL, "failed to raise signal %d",
		                     number);
		raise(SIGKILL);
	}
}

#define TOKEN(a, b, c, d) \
	(((uint8_t) (a) << 24) | \
	 ((uint8_t) (b) << 16) | \
	 ((uint8_t) (c) <<  8) | \
	 ((uint8_t) (d)      ))

static const XecSignalToken signal_token_table[] = {
	[SIGABRT]	= TOKEN('A', 'B', 'R', 'T'),
	[SIGALRM]	= TOKEN('A', 'L', 'R', 'M'),
	[SIGBUS]	= TOKEN('B', 'U', 'S', ' '),
	[SIGCHLD]	= TOKEN('C', 'H', 'L', 'D'),
	[SIGCLD]	= TOKEN('C', 'L', 'D', ' '),
	[SIGCONT]	= TOKEN('C', 'O', 'N', 'T'),
	[SIGFPE]	= TOKEN('F', 'P', 'E', ' '),
	[SIGHUP]	= TOKEN('H', 'U', 'P', ' '),
	[SIGILL]	= TOKEN('I', 'L', 'L', ' '),
	[SIGINT]	= TOKEN('I', 'N', 'T', ' '),
	[SIGIO]		= TOKEN('I', 'O', ' ', ' '),
	[SIGIOT]	= TOKEN('I', 'O', 'T', ' '),
	[SIGKILL]	= TOKEN('K', 'I', 'L', 'L'),
	[SIGPIPE]	= TOKEN('P', 'I', 'P', 'E'),
	[SIGPOLL]	= TOKEN('P', 'O', 'L', 'L'),
	[SIGPROF]	= TOKEN('P', 'R', 'O', 'F'),
	[SIGPWR]	= TOKEN('P', 'W', 'R', ' '),
	[SIGQUIT]	= TOKEN('Q', 'U', 'I', 'T'),
	[SIGSEGV]	= TOKEN('S', 'E', 'G', 'V'),
	[SIGSTKFLT]	= TOKEN('S', 'T', 'K', 'F'),
	[SIGSTOP]	= TOKEN('S', 'T', 'O', 'P'),
	[SIGSYS]	= TOKEN('S', 'Y', 'S', ' '),
	[SIGTERM]	= TOKEN('T', 'E', 'R', 'M'),
	[SIGTRAP]	= TOKEN('T', 'R', 'A', 'P'),
	[SIGTSTP]	= TOKEN('T', 'S', 'T', 'P'),
	[SIGTTIN]	= TOKEN('T', 'T', 'I', 'N'),
	[SIGTTOU]	= TOKEN('T', 'T', 'O', 'U'),
	[SIGUNUSED]	= TOKEN('U', 'N', 'U', 'S'),
	[SIGURG]	= TOKEN('U', 'R', 'G', ' '),
	[SIGUSR1]	= TOKEN('U', 'S', 'R', '1'),
	[SIGUSR2]	= TOKEN('U', 'S', 'R', '2'),
	[SIGVTALRM]	= TOKEN('V', 'T', 'A', 'L'),
	[SIGWINCH]	= TOKEN('W', 'I', 'N', 'C'),
	[SIGXCPU]	= TOKEN('X', 'C', 'P', 'U'),
	[SIGXFSZ]	= TOKEN('X', 'F', 'S', 'Z'),
};

XecSignalToken xec_signal_to_token(int number)
{
	if (number < 0 || number >= ARRAY_SIZE(signal_token_table))
		return 0;

	return signal_token_table[number];
}

int xec_signal_from_token(XecSignalToken token)
{
	for (int i = 0; i < ARRAY_SIZE(signal_token_table); i++)
		if (signal_token_table[i] == token)
			return i;

	return -1;
}
