/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_HOST_MESSAGE_H
#define XEC_HOST_MESSAGE_H

#include <xec/error.h>
#include <xec/packet.h>

void xec_host_message_handle(int fd, XecPacketMessage *, XecError *);

#endif
