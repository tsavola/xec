/*
 * Copyright (c) 2008 Timo Savola
 */

#include "conn.h"

#include <poll.h>
#include <unistd.h>

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/fuse.h>
#include <xec/io.h>
#include <xec/packet.h>
#include <xec/poll.h>
#include <xec/host/addr.h>
#include <xec/host/message.h>
#include <xec/host/agent/fuse.h>

static XecPacketByteorder conn_byteorder;
static XecPoll *conn_poll;
static XecFuseBuffer conn_buf;

int xec_host_agent_conn_handshake(XecError *error)
{
	union {
		XecPacket head;
		XecPacketMessage message;
		XecPacketFuseInit init;
		XecPacketFuseStart start;
	} packet = {};
	int fd;

	xec_debug();

	fd = xec_host_addr_connect(error);
	if (error->set)
		return -1;

	packet.head.size = sizeof (packet.init);
	packet.head.type = XEC_PACKET_FUSE_INIT;

	xec_packet_write(fd, &packet.head, error);

	while (!error->set) {
		xec_packet_read(fd, &packet.head, sizeof (packet), error);
		if (error->set)
			break;

		switch (packet.head.type) {
		case XEC_PACKET_MESSAGE:
			xec_host_message_handle(fd, &packet.message, error);
			break;

		case XEC_PACKET_FUSE_START:
			conn_byteorder = packet.start.byteorder;
			return fd;

		default:
			xec_packet_skip_unexpected(fd, &packet.head, error);
			break;
		}
	}

	xec_io_close(fd);
	return -1;
}

static void conn_handle(XecPoll *poll, XecPollMask events, XecError *error)
{
	xec_debug();

	xec_poll_check(events, error);
	if (error->set)
		goto close;

	xec_io_read(poll->fd, conn_buf.data, sizeof (conn_buf), error);
	if (error->set)
		goto close;

	xec_host_agent_fuse_handle(poll->fd, &conn_buf, error);
	if (error->set)
		goto close;

	xec_io_write(poll->fd, conn_buf.data, conn_buf.out.len, error);
	if (error->set)
		goto close;

	return;

close:
	xec_poll_destroy(poll);
}

void xec_host_agent_conn_init(int fd, XecError *error)
{
	xec_debug();

	conn_poll = xec_poll_add(fd, POLLIN, conn_handle, NULL, error);
	if (error->set)
		return;

	xec_host_agent_fuse_init(conn_byteorder, error);
}

void xec_host_agent_conn_close(XecError *error)
{
	xec_debug();

	xec_host_agent_fuse_shutdown(error);
	xec_poll_destroy(conn_poll);
}
