/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_HOST_AGENT_REF_H
#define XEC_HOST_AGENT_REF_H

#include <xec/error.h>

void xec_host_agent_ref_init(int sock_fd, int initial_fd, XecError *);

#endif
