/*
 * Copyright (c) 2008 Timo Savola
 */

#include "ref.h"

#include <stdbool.h>
#include <stddef.h>

#include <netdb.h>
#include <poll.h>
#include <unistd.h>

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/io.h>
#include <xec/poll.h>
#include <xec/host/agent/conn.h>

static XecPoll *ref_listen_poll;
static int ref_count;

static void ref_conn_handle(XecPoll *poll, XecPollMask events,
                            XecError *global_error)
{
	XecError error = {};
	bool eof = false;
	char buf;

	eof = xec_poll_check_eof(events, &error);
	if (eof || error.set)
		goto unref;

	eof = xec_io_read_eof(poll->fd, &buf, sizeof (buf), &error);
	if (eof || error.set)
		goto unref;

	xec_debug("got dummy data");
	return;

unref:
	if (eof)
		xec_debug("eof");

	ref_count--;
	xec_debug("ref_count=%d", ref_count);

	xec_poll_destroy(poll);

	/* TODO: timeout */
	if (ref_count == 0) {
		xec_poll_destroy(ref_listen_poll);
		xec_host_agent_conn_close(global_error);
	}
}

static void ref_listen_handle(XecPoll *listen_poll, XecPollMask events,
                              XecError *global_error)
{
	XecError error = {};
	struct sockaddr_storage addr;
	socklen_t addrlen = sizeof (addr);
	int conn_fd;

	xec_poll_check(events, global_error);
	if (global_error->set)
		return;

	conn_fd = accept(listen_poll->fd, (struct sockaddr *) &addr, &addrlen);
	if (conn_fd < 0) {
		xec_error_set_errno(&error, "accept");
		return;
	}

	xec_debug("conn_fd=%d", conn_fd);

	xec_poll_add(conn_fd, POLLIN, ref_conn_handle, NULL, &error);
	if (error.set)
		goto no_poll;

	ref_count++;
	xec_debug("ref_count=%d", ref_count);
	return;

no_poll:
	xec_io_close(conn_fd);
}

void xec_host_agent_ref_init(int listen_fd, int initial_fd, XecError *error)
{
	xec_debug();

	xec_poll_add(initial_fd, POLLIN, ref_conn_handle, NULL, error);
	if (error->set)
		return;

	ref_count++;
	xec_debug("ref_count=%d", ref_count);

	ref_listen_poll = xec_poll_add(listen_fd, POLLIN, ref_listen_handle,
	                               NULL, error);
}
