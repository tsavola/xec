/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_HOST_AGENT_CONN_H
#define XEC_HOST_AGENT_CONN_H

#include <xec/attr.h>
#include <xec/error.h>

int ATTR_USE_RESULT xec_host_agent_conn_handshake(XecError *);
void xec_host_agent_conn_init(int fd, XecError *);
void xec_host_agent_conn_close(XecError *);

#endif
