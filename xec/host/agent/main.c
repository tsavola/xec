/*
 * Copyright (c) 2008 Timo Savola
 */

#define _GNU_SOURCE

#include "main.h"

#include <errno.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/poll.h>
#include <xec/prog.h>
#include <xec/host/addr.h>
#include <xec/host/agent/conn.h>
#include <xec/host/agent/ref.h>

#define SOCKET_PATH	"/xec-agent"

bool xec_host_agent_main(XecError *error)
{
	bool agent = false;
	struct sockaddr_un addr = {
		.sun_family	= AF_UNIX,
	};
	int sock_fd;
	int conn_fd;
	int ref_fd[2];
	pid_t pid;

	xec_debug();

	/* use abstract namespace (nul-initiated, not nul-terminated) */
	strncpy(addr.sun_path + 1, SOCKET_PATH, sizeof (addr.sun_path) - 1);

	sock_fd = socket(PF_UNIX, SOCK_STREAM, 0);
	if (sock_fd < 0) {
		xec_error_set_errno(error, "socket");
		goto no_socket;
	}

	/* bind or connect - whichever succeeds first.  this is an infinite
	 * loop if the lifetimes of all agent instances always fall between our
	 * bind and connect calls, but that should never happen...
	 *
	 * - if we are bound, we will fork a new agent.
	 *
	 * - if we are connected, we are only a front.  the connection becomes
	 *   our reference to the existing agent.
	 */
	while (true) {
		if (bind(sock_fd, (struct sockaddr *) &addr,
		         sizeof (addr)) == 0) {
			xec_debug("bound");
			break;
		}

		if (errno != EADDRINUSE) {
			xec_error_set_errno(error, "bind");
			goto no_bind_or_connect;
		}

		if (connect(sock_fd, (struct sockaddr *) &addr,
		            sizeof (addr)) == 0) {
			xec_debug("connected");
			return false;
		}

		if (errno != ECONNREFUSED) {
			xec_error_set_errno(error, "connect");
			goto no_bind_or_connect;
		}
	}

	if (listen(sock_fd, SOMAXCONN) < 0) {
		xec_error_set_errno(error, "listen");
		goto no_listen;
	}

	conn_fd = xec_host_agent_conn_handshake(error);
	if (error->set)
		goto no_conn;

	/* reference between the front and the (soon to be created) agent */
	if (pipe(ref_fd) < 0) {
		xec_error_set_errno(error, "pipe");
		goto no_ref;
	}

	pid = fork();
	if (pid < 0) {
		xec_error_set_errno(error, "fork");
		goto no_fork;
	}

	if (pid == 0) {
		xec_debug_domain("agent");
		xec_debug();

		agent = true;

		xec_host_addr_free();

		setsid();
		chdir("/");
		umask(0);

		/* this also takes care of closing our copy of ref_fd[1] */
		for (int fd = getdtablesize(); --fd >= 0; )
			if (fd != sock_fd && fd != conn_fd &&
			    fd != ref_fd[0] && !xec_debug_fd(fd))
				close(fd);

		xec_poll_init(error);
		if (error->set)
			goto no_poll_init;

		xec_host_agent_ref_init(sock_fd, ref_fd[0], error);
		if (error->set)
			goto no_ref_init;

		xec_host_agent_conn_init(conn_fd, error);
		if (error->set)
			goto no_conn_init;

		xec_poll_loop(error);
	}

no_conn_init:
no_ref_init:
no_poll_init:
no_fork:
	close(ref_fd[0]);
	/* don't close ref_fd[1] - it's our agent reference */
no_ref:
	close(conn_fd);
no_conn:
no_listen:
no_bind_or_connect:
	close(sock_fd);
no_socket:
	return agent;
}
