/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_HOST_AGENT_FUSE_H
#define XEC_HOST_AGENT_FUSE_H

#include <xec/error.h>
#include <xec/fuse.h>
#include <xec/packet.h>

void xec_host_agent_fuse_init(XecPacketByteorder, XecError *);
void xec_host_agent_fuse_handle(int fd, XecFuseBuffer *, XecError *);
void xec_host_agent_fuse_shutdown(XecError *);

#endif
