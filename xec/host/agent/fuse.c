/*
 * Copyright (c) 2008 Timo Savola
 */

#include "fuse.h"

#include <errno.h>
#include <stdint.h>

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/fuse.h>
#include <xec/io.h>

void xec_host_agent_fuse_init(XecPacketByteorder byteorder, XecError *error)
{
	xec_debug();
}

void xec_host_agent_fuse_handle(int fd, XecFuseBuffer *buf, XecError *error)
{
	struct fuse_out_header out = {
		.len	= sizeof (out),
		.error	= -ENODEV,
		.unique	= buf->in.unique,
	};

	xec_debug();
	xec_io_write(fd, &out, out.len, error);

	/* TODO: fuse */
}

void xec_host_agent_fuse_shutdown(XecError *error)
{
	xec_debug();
}
