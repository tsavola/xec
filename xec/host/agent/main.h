/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_HOST_AGENT_MAIN_H
#define XEC_HOST_AGENT_MAIN_H

#include <stdbool.h>

#include <xec/attr.h>
#include <xec/error.h>

bool ATTR_USE_RESULT xec_host_agent_main(XecError *);

#endif
