/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_HOST_ADDR_H
#define XEC_HOST_ADDR_H

#include <xec/error.h>

void xec_host_addr_init(XecError *);
void xec_host_addr_free(void);

int xec_host_addr_connect(XecError *);

#endif
