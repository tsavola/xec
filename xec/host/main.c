/*
 * Copyright (c) 2008 Timo Savola
 */

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/main.h>
#include <xec/prog.h>
#include <xec/host/addr.h>
#include <xec/host/agent/main.h>
#include <xec/host/front/main.h>

void xec_main(int argc, char **argv, XecError *error)
{
	char *cwd, **args;
	bool agent;

	xec_debug_domain("start");
	xec_debug();

	if (argc < 2) {
		xec_prog_usage("CWD ARGS...");
		return;
	}

	cwd = argv[0];
	args = argv + 1;

	xec_host_addr_init(error);
	if (error->set)
		return;

	agent = xec_host_agent_main(error);
	if (agent || error->set)
		return;

	xec_host_front_main(cwd, args, error);
}
