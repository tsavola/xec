/*
 * Copyright (c) 2008 Timo Savola
 */

#define _POSIX_SOURCE

#include "addr.h"

#include <errno.h>

#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <xec/debug.h>
#include <xec/error.h>

#define ADDR_NODE	"localhost"
#define ADDR_SERVICE	"1203"

static struct addrinfo *addr_info;

void xec_host_addr_init(XecError *error)
{
	struct addrinfo hints = {
		.ai_flags	= AI_ADDRCONFIG,
		.ai_socktype	= SOCK_STREAM,
	};
	int ret;

	ret = getaddrinfo(ADDR_NODE, ADDR_SERVICE, &hints, &addr_info);
	if (ret < 0)
		xec_error_setf(error, "getaddrinfo: %s", gai_strerror(ret));
}

void xec_host_addr_free(void)
{
	xec_debug();

	freeaddrinfo(addr_info);
	addr_info = NULL;
}

int xec_host_addr_connect(XecError *error)
{
	int fd;
	int last_errno = 0;

	xec_debug();

	for (struct addrinfo *i = addr_info; i; i = i->ai_next) {
		fd = socket(i->ai_family, i->ai_socktype, i->ai_protocol);
		if (fd < 0) {
			last_errno = errno;
			continue;
		}

		do {
			if (connect(fd, i->ai_addr, i->ai_addrlen) == 0) {
				xec_debug("fd=%d", fd);
				return fd;
			}
		} while (errno == EINTR);
		last_errno = errno;

		close(fd);
	}

	xec_error_no(error, last_errno);
	return -1;
}
