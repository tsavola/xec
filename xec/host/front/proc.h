/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_HOST_FRONT_PROC_H
#define XEC_HOST_FRONT_PROC_H

#include <xec/error.h>

void xec_host_front_proc_init(char *cwd, char **args, char **env, XecError *);
void xec_host_front_proc_exit(void);

void xec_host_front_proc_files_initialized(XecError *);

#endif
