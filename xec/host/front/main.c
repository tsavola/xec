#include "main.h"

extern char **environ;

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/poll.h>
#include <xec/host/addr.h>
#include <xec/host/front/file.h>
#include <xec/host/front/proc.h>

void xec_host_front_main(char *cwd, char **args, XecError *error)
{
	xec_debug_domain("front");
	xec_debug();

	xec_poll_init(error);
	if (error->set)
		return;

	xec_host_front_proc_init(cwd, args, environ, error);
	if (error->set)
		return;

	xec_host_front_file_init(error);
	if (error->set)
		return;

	xec_host_addr_free();

	xec_poll_loop(error);
	if (error->set)
		return;

	xec_host_front_proc_exit();
}
