/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_HOST_FRONT_MAIN_H
#define XEC_HOST_FRONT_MAIN_H

#include <xec/error.h>

const char *xec_host_front_pid(void);
const char *xec_host_front_cwd(void);
const char *const *xec_host_front_args(void);
const char *const *xec_host_front_environ(void);

void xec_host_front_main(char *cwd, char **args, XecError *);

#endif
