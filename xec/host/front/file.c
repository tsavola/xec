/*
 * Copyright (c) 2008 Timo Savola
 */

#include "file.h"

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

#include <poll.h>
#include <unistd.h>

#include <xec/debug.h>
#include <xec/error.h>
#include <xec/io.h>
#include <xec/packet.h>
#include <xec/poll.h>
#include <xec/host/addr.h>
#include <xec/host/message.h>
#include <xec/host/front/proc.h>

static char file_buf[4096];
static int file_pending;

static void file_conn_handle(XecPoll *poll, XecPollMask events,
                             XecError *error)
{
	int file_fd = (intptr_t) poll->data;
	size_t len;

	xec_debug();

	len = xec_io_read_some(poll->fd, file_buf, sizeof (file_buf),
	                       error);
	if (error->set)
		return;

	if (len == 0)
		goto close;

	/* TODO: non-blocking output */
	xec_io_write(file_fd, file_buf, len, error);
	return;

close:
	xec_poll_destroy(poll);
}

static void file_conn_handle_handshake(XecPoll *poll, XecPollMask events,
                                       XecError *error)
{
	union {
		XecPacket head;
		XecPacketMessage message;
		XecPacketFileStart start;
	} packet;

	xec_debug();

	xec_poll_check(events, error);
	if (error->set)
		return;

	xec_packet_read(poll->fd, &packet.head, sizeof (packet), error);
	if (error->set)
		return;

	switch (packet.head.type) {
	case XEC_PACKET_MESSAGE:
		xec_host_message_handle(poll->fd, &packet.message, error);
		break;

	case XEC_PACKET_FILE_START:
		if (--file_pending == 0) {
			xec_host_front_proc_files_initialized(error);
			if (error->set)
				return;
		}

		poll->func = file_conn_handle;
		break;

	default:
		xec_packet_skip_unexpected(poll->fd, &packet.head, error);
		break;
	}
}

static void file_conn_init(int file_fd, XecError *error)
{
	XecPacketFileInit packet = {
		.head = {
			.size	= sizeof (packet),
			.type	= XEC_PACKET_FILE_INIT,
		},
		.front		= getpid(),
		.fd		= file_fd,
	};
	int conn_fd;

	xec_debug("file_fd=%d", file_fd);

	conn_fd = xec_host_addr_connect(error);
	if (error->set)
		return;

	xec_debug("conn_fd=%d", conn_fd);

	xec_packet_write(conn_fd, &packet.head, error);
	if (error->set)
		return;

	xec_poll_add(conn_fd, POLLIN, file_conn_handle_handshake,
	             (void *) (intptr_t) file_fd, error);
	if (error->set)
		return;

	file_pending++;
}

void xec_host_front_file_init(XecError *error)
{
	xec_debug();

	/* TODO: support arbitrary file descriptors */

	for (int fd = 0; fd <= 2; fd++) {
		file_conn_init(fd, error);
		if (error->set)
			return;

		/* TODO: poll fds */
	}
}
