#include "proc.h"

#include <signal.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <poll.h>
#include <unistd.h>

extern char **environ;

#include <xec/array.h>
#include <xec/debug.h>
#include <xec/error.h>
#include <xec/packet.h>
#include <xec/poll.h>
#include <xec/signal.h>
#include <xec/host/addr.h>
#include <xec/host/message.h>
#include <xec/host/front/file.h>

typedef void (*ProcSendFunc)(XecError *);

static char *proc_cwd;
static char **proc_args;
static char **proc_env;

static int proc_fd;

static bool proc_spawn_after_files;
static bool proc_spawn_after_settings;

static int proc_exit_signal = -1;
static int proc_exit_value = -1;

static void proc_set_strings(XecPacketProcKey key, char **strings,
                             XecError *error)
{
	int count = 0;
	size_t size = 0;

	for (char **s = strings; *s; s++) {
		size += strlen(*s) + 1;
		count++;
	}

	char buf[sizeof (XecPacketProcSet) + size];
	XecPacketProcSet *packet = (XecPacketProcSet *) buf;

	packet->head.size = sizeof (buf);
	packet->head.type = XEC_PACKET_PROC_SET;
	packet->key = key;
	packet->count = count;

	char *data = packet->data;

	for (char **s = strings; *s; s++) {
		size_t len;

		len = strlen(*s) + 1;
		memcpy(data, *s, len);
		data += len;
	}

	xec_packet_write(proc_fd, &packet->head, error);
}

static void proc_set_cwd(XecError *error)
{
	char *vector[] = { proc_cwd, NULL };

	xec_debug();
	proc_set_strings(XEC_PACKET_PROC_SET_CWD, vector, error);
}

static void proc_set_args(XecError *error)
{
	xec_debug();
	proc_set_strings(XEC_PACKET_PROC_SET_ARGS, proc_args, error);
}

static void proc_set_env(XecError *error)
{
	xec_debug();
	proc_set_strings(XEC_PACKET_PROC_SET_ENV, proc_env, error);
}

static void proc_spawn(XecError *error)
{
	XecPacketProcSpawn packet = {
		.head = {
			.size	= sizeof (packet),
			.type	= XEC_PACKET_PROC_SPAWN,
		},
	};

	xec_debug();
	xec_packet_write(proc_fd, &packet.head, error);
}

static void proc_spawn_if_files_initialized(XecError *error)
{
	xec_debug();

	if (proc_spawn_after_settings)
		proc_spawn(error);
	else
		proc_spawn_after_files = true;
}

void xec_host_front_proc_files_initialized(XecError *error)
{
	xec_debug();

	if (proc_spawn_after_files)
		proc_spawn(error);
	else
		proc_spawn_after_settings = true;
}

static void proc_handle(XecPoll *poll, XecPollMask events, XecError *error)
{
	union {
		XecPacket head;
		XecPacketMessage message;
		XecPacketProcStatus status;
	} packet;

	xec_debug();

	xec_poll_check(events, error);
	if (error->set)
		return;

	xec_packet_read(poll->fd, &packet.head, sizeof (packet), error);
	if (error->set)
		return;

	switch (packet.head.type) {
	case XEC_PACKET_MESSAGE:
		xec_debug("message");
		xec_host_message_handle(poll->fd, &packet.message, error);
		break;

	case XEC_PACKET_PROC_STATUS:
		xec_debug("status");

		if (packet.status.signal) {
			xec_debug("signal");
			proc_exit_signal = xec_signal_from_token(
				packet.status.signal);
		} else {
			xec_debug("value");
			proc_exit_value = packet.status.value;
		}

		goto close;

	default:
		xec_packet_skip_unexpected(poll->fd, &packet.head, error);
		break;
	}

	return;

close:
	xec_poll_destroy(poll);
	xec_signal_ignore(error);
}

static const ProcSendFunc proc_send_queue[] = {
	proc_set_cwd,
	proc_set_args,
	proc_set_env,
	/* TODO: set ids */
	/* TODO: set rlimits */
	proc_spawn_if_files_initialized,
};

static void proc_handle_send(XecPoll *poll, XecPollMask events,
                             XecError *error)
{
	static int next;

	xec_debug();

	if (events & POLLIN) {
		proc_handle(poll, events, error);
	} else if (events & POLLOUT) {
		xec_poll_check(events, error);
		if (error->set)
			return;

		proc_send_queue[next](error);
		if (error->set)
			return;

		if (++next == ARRAY_SIZE(proc_send_queue)) {
			xec_poll_modify(poll, POLLIN, error);
			poll->func = proc_handle;
		}
	}
}

static void proc_signal(int signal, XecError *error)
{
	XecPacketProcSignal packet = {
		.head = {
			.size	= sizeof (packet),
			.type	= XEC_PACKET_PROC_SIGNAL,
		},
		.signal		= xec_signal_to_token(signal),
	};

	xec_debug("signal=%d", signal);
	xec_packet_write(proc_fd, &packet.head, error);
}

static void proc_connect(XecError *error)
{
	XecPacketProcInit packet = {
		.head = {
			.size	= sizeof (packet),
			.type	= XEC_PACKET_PROC_INIT,
		},
		.front		= getpid(),
	};

	xec_debug();

	proc_fd = xec_host_addr_connect(error);
	if (error->set)
		return;

	xec_debug("proc_fd=%d", proc_fd);

	xec_packet_write(proc_fd, &packet.head, error);
	if (error->set)
		return;

	xec_poll_add(proc_fd, POLLOUT | POLLIN, proc_handle_send, NULL, error);
}

void xec_host_front_proc_init(char *cwd, char **args, char **env,
                              XecError *error)
{
	XecSignalMask mask;

	xec_debug();

	proc_cwd = cwd;
	proc_args = args;
	proc_env = env;

	mask = XEC_SIGNAL_ALL & ~XEC_SIGNAL_BIT(SIGCHLD);
	xec_signal_handle(mask, proc_signal, error);
	if (error->set)
		return;

	proc_connect(error);
}

void xec_host_front_proc_exit(void)
{
	if (proc_exit_signal >= 0)
		xec_signal_raise(proc_exit_signal);

	if (proc_exit_value >= 0)
		exit(proc_exit_value);
}
