/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_HOST_FRONT_FILE_H
#define XEC_HOST_FRONT_FILE_H

#include <xec/error.h>

void xec_host_front_file_init(XecError *);

#endif
