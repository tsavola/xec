/*
 * Copyright (c) 2008 Timo Savola
 */

#include "message.h"

#include <xec/error.h>
#include <xec/io.h>
#include <xec/prog.h>

void xec_host_message_handle(int fd, XecPacketMessage *message,
                             XecError *error)
{
	char buf[message->head.size - sizeof (XecPacketMessage)];

	xec_io_read(fd, buf, sizeof (buf), error);
	if (error->set)
		return;

	if (buf[sizeof (buf) - 1] != '\0') {
		xec_error_set(error, "message data not nul-terminated");
		return;
	}

	if (message->error)
		xec_error_setf(error, "remote: %s", buf);
	else
		xec_prog_messagef("remote: %s", buf);
}
