/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_FUSE_H
#define XEC_FUSE_H

#include <linux/fuse.h>

typedef union XecFuseBuffer	XecFuseBuffer;

union XecFuseBuffer {
	struct fuse_in_header in;
	struct fuse_out_header out;

	char data[FUSE_MIN_READ_BUFFER];
};

#endif
