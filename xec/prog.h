/*
 * Copyright (c) 2008 Timo Savola
 */

#ifndef XEC_PROG_H
#define XEC_PROG_H

const char *xec_prog_name(void);
void xec_prog_usage(const char *args);
void xec_prog_messagef(const char *format, ...);

#endif
