CPPFLAGS	+= -I.
CFLAGS		+= -g -Wall -Wmissing-prototypes
CCFLAGS		+= -std=c99

BINARIES	:= host slave
LIBRARIES	:= common
NODIST		:= $(LIBRARIES)

build: $(BINARIES)
xec:: host
xec-slave:: slave

$(BINARIES): common-static

initramfs:: slave
	$(QUIET) fakeroot $(MAKE) -f initramfs.mk

include build/project.mk

CTAGS		:= ctags-exuberant
ETAGS		:= $(CTAGS) -e

TAGS_SOURCES	:= $(shell find xec -name "*.c")

TAGS: $(TAGS_SOURCES)
	$(call echo,Generate,$@)
	$(QUIET) $(ETAGS) $(TAGS_SOURCES)

tags: $(TAGS_SOURCES)
	$(call echo,Generate,$@)
	$(QUIET) $(CTAGS) $(TAGS_SOURCES)

todo::
	$(QUIET) find xec -name "*.[ch]" | xargs grep -n -i todo | sed -r \
		-e "s,[[:space:]]*/\*,," \
		-e "s,[[:space:]]*\*/,,"
